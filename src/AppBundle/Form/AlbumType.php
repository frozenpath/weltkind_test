<?php

namespace AppBundle\Form;

use AppBundle\Entity\Album;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Название альбома'
        ]);
        $builder->add('description', TextareaType::class, [
            'label' => 'Описание'
        ]);
        $builder->add('created', DateType::class, [
            'label' => 'Дата',
            'data' => new \DateTime(),
            'widget' => 'single_text'
        ]);

        $builder->add('images', CollectionType::class, array(
            'entry_type' => PhotoType::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
            'by_reference' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Album::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_album_type';
    }
}
