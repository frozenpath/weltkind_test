<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\AlbumType;
use AppBundle\Form\NumerationType;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $albums = $this->getDoctrine()->getRepository(Photo::class)->getAlbums();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $albums,
            $request->query->getInt('page', 1)/*page number*/,
            12
        );

        return $this->render('AppBundle:Main:index.html.twig', array(
            'albums' => $albums,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/album/{album_id}")
     * @param int $album_id
     * @return Response
     */
    public function showAlbumAction(int $album_id)
    {
        $album = $this->getDoctrine()->getRepository(Album::class)->find($album_id);

        //$images = $this->getDoctrine()->getRepository(Photo::class)->findBy(['album' => $album_id]);
        $images = $this->getDoctrine()->getRepository(Photo::class)->getPhotos($album_id);

        $numeration_form = $this->createForm(NumerationType::class);

        return $this->render('AppBundle:Main:album.html.twig', array(
            'album' => $album,
            'images' => $images,
            'numeration_form' => $numeration_form

        ));
    }

    /**
     * @Route("/position/{album_id}")
     * @param Request $request
     * @param int $album_id
     * @return Response
     */
    public function savePositionAction(Request $request, int $album_id)
    {
        $album = $this->getDoctrine()->getRepository(Album::class)->find($album_id);

        $form = $this->createForm(NumerationType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($album);
            $em->flush();
        }
        return $this->redirectToRoute('app_main_admin');
    }

    /**
     * @Route("/position-photo/{photo_id}")
     * @param Request $request
     * @param int $photo_id
     * @return Response
     */
    public function savePhotoPositionAction(Request $request, int $photo_id)
    {
        $photo = $this->getDoctrine()->getRepository(Photo::class)->find($photo_id);

        $form = $this->createForm(NumerationType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($photo);
            $em->flush();
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
        //return $this->redirectToRoute($request->headers->get('referer'));
    }

    /**
     * @Route("/login")
     */
    public function loginAction()
    {
        $authUtils = $this->get('security.authentication_utils');
        $error = $authUtils->getLastAuthenticationError();

        $lastUsername = $authUtils->getLastUsername();

        return $this->render('AppBundle:Main:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/admin")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function adminAction(Request $request)
    {
        $all_albums = $this->getDoctrine()->getRepository(Album::class)->getAllAlbums();
        $album = new Album();

        $form = $this->createForm(AlbumType::class, $album);
        $numeration_form = $this->createForm(NumerationType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $album->getImages();
            $album->setCreated(new \DateTime());

            if ($images) {
                foreach ($images as $image) {
                    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
                    $file = $image->getImage();
                    $image->setAlbum($album);
                    $image->setCreated(new \DateTime());
                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('album_image_directory'), $filename);
                    $image->setImage($filename);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();
            return $this->redirectToRoute('app_main_admin');
        }

        return $this->render('AppBundle:Main:admin.html.twig', array(
            'form' => $form->createView(),
            'numeration_form' => $numeration_form,
            'albums' => $all_albums
        ));
    }
}
